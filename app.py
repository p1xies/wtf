from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello():
	return "<h1>Ola Mundo</h1>", 200


if __name__ == '__main__':
	app.run()
